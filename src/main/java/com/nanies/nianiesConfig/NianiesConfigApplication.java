package com.nanies.nianiesConfig;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableConfigServer
public class NianiesConfigApplication {

	public static void main(String[] args) {
		SpringApplication.run(NianiesConfigApplication.class, args);
	}
}
